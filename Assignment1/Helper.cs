﻿using System.Globalization;
using System.Text;
using System;
using System.IO;

namespace Assignment1
{
    public class Helper
    {
        public static bool KiemTraDinhDangNamSinh(string namSinh)
        {
            string[] danhSachDinhDang = { "dd/MM/yyyy", "dd/M/yyyy", "d/MM/yyyy", "d/M/yyyy" };

            foreach (string dinhDang in danhSachDinhDang)
            {
                if (DateTime.TryParseExact(namSinh, dinhDang, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ngayChuyenDoi))
                {
                    DateTime ngayHienTai = DateTime.Now;
                    if (ngayHienTai.Year - ngayChuyenDoi.Year >= 18)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static DateTime DinhDangNamSinh(string namSinh)
        {
            string[] danhSachDinhDang = { "dd/MM/yyyy", "dd/M/yyyy", "d/MM/yyyy", "d/M/yyyy" };

            DateTime ketQua = DateTime.MinValue;
            if (DateTime.TryParseExact(namSinh, danhSachDinhDang, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ngaySinhDaDinhDang))
            {
                ketQua = ngaySinhDaDinhDang;
            }
            return ketQua;
        }

    }
}
