﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class GiaoVien: NguoiLaoDong
    {
        private double HeSoLuong { get; set; }
        public GiaoVien() { }
        public GiaoVien(string hoTen, DateTime namSinh, double luongCoBan, double heSoLuong):base(hoTen, namSinh, luongCoBan) {
            HeSoLuong = heSoLuong;
        }
        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }
        public double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public void XuatThongTin()
        {
            string namSinhDinhDang = $"{NamSinh.Day.ToString("00")}/{NamSinh.Month.ToString("00")}/{NamSinh.Year}";
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {namSinhDinhDang}, " +
                $"luong co ban: {LuongCoBan}, he so luong: {HeSoLuong}, luong: {TinhLuong()}.");
        }

        public double XuLy()
        {
            return HeSoLuong + 0.6;
        }

    }
}
