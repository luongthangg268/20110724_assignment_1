﻿using System.Text;
using System.Globalization;
using System.IO;
using System;

namespace Assignment1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            List<GiaoVien> danhSachGV = new List<GiaoVien>();

            string kiemTraSoLuongGV;
            Console.Write("Nhập Số lượng giáo viên muốn nhập thông tin: ");
            do
            {
                kiemTraSoLuongGV = Console.ReadLine();
                if (!int.TryParse(kiemTraSoLuongGV, out _)) {
                    Console.Write("Vui lòng chỉ nhập ký tự số. Nhập lại: ");
                }
            }
            while (!int.TryParse(kiemTraSoLuongGV, out _));
            int soLuongGV = int.Parse(kiemTraSoLuongGV);

            int soLuongGVDaNhap = 0;
            while (soLuongGVDaNhap < soLuongGV)
            {
                Console.WriteLine("Nhập thông tin giáo viên");
                Console.Write("Nhập họ tên: ");
                string hoTen;
                do
                {
                    hoTen = Console.ReadLine();
                    if (hoTen == "")
                        Console.Write("Không được bỏ trống thông tin. Vui lòng nhập lại: ");
                }
                while (hoTen == "");

                Console.Write("Nhập năm sinh: ");
                string namSinh;
                bool kiemTraDinhDang;
                do
                {
                    namSinh = Console.ReadLine();
                    kiemTraDinhDang = Helper.KiemTraDinhDangNamSinh(namSinh);
                    if (!kiemTraDinhDang) {
                        Console.Write("Vui lòng nhập định dạng ngày/tháng/năm và từ 18 tuổi trở lên. Nhập lại: ");
                    }
                }
                while (!kiemTraDinhDang);

                Console.Write("Nhập lương cơ bản: ");
                string kiemTraLuongCoBan;
                do
                {
                    kiemTraLuongCoBan = Console.ReadLine();
                    if (!double.TryParse(kiemTraLuongCoBan, out _)) {
                        Console.Write("Thông tin lương cơ bản không hợp lệ! Nhập lại: ");
                    }  
                }
                while (!double.TryParse(kiemTraLuongCoBan, out _));
                double luongCoBan = double.Parse(kiemTraLuongCoBan);

                Console.Write("Nhập hệ số lương (Nên từ 1.00 -> 8.00): ");
                string kiemTraHeSoLuong;
                do
                {
                    kiemTraHeSoLuong = Console.ReadLine();
                    if (!double.TryParse(kiemTraHeSoLuong, NumberStyles.Float, CultureInfo.InvariantCulture, out _)) {
                        Console.Write("Thông tin hệ số lương không hợp lệ! Nhập lại: ");
                    }
                }
                while (!double.TryParse(kiemTraHeSoLuong, NumberStyles.Float, CultureInfo.InvariantCulture, out _));
                double heSoLuong = double.Parse(kiemTraHeSoLuong);

                GiaoVien gv = new GiaoVien();
                gv.NhapThongTin(hoTen, Helper.DinhDangNamSinh(namSinh), luongCoBan);
                gv.NhapThongTin(heSoLuong);
                danhSachGV.Add(gv);

                Console.Clear();

                Console.WriteLine("Danh sách tất cả giáo viên");
                danhSachGV.ForEach(gv => gv.XuatThongTin());

                soLuongGVDaNhap++;
            }
            Console.WriteLine("\nThông tin giáo viên có lương thấp nhất");
            try
            {
                danhSachGV.OrderBy(gv => gv.TinhLuong()).FirstOrDefault().XuatThongTin();
                Console.Read();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Không có thông tin giáo viên nào!");
            }
        }
    }
}