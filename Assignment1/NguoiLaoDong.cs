﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class NguoiLaoDong
    {
        protected string HoTen { get; set; }
        protected DateTime NamSinh { get; set; }
        protected double LuongCoBan { get; set; }
        public NguoiLaoDong() { }
        public NguoiLaoDong(string hoTen, DateTime namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }
        public void NhapThongTin(string hoTen, DateTime namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }
        public double TinhLuong()
        {
            return LuongCoBan;
        }
        public void XuatThongtin()
        {
            string namSinhDinhDang = $"{NamSinh.Day.ToString("00")}/{NamSinh.Month.ToString("00")}/{NamSinh.Year}";
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {namSinhDinhDang}, " +
                $"luong co ban: {LuongCoBan}.");
        
        }
    }
}
